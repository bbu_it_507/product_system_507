package com.example.product_system_507;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductSystem507Application {

    public static void main(String[] args) {
        SpringApplication.run(ProductSystem507Application.class, args);
    }

}
